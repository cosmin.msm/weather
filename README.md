# weather

Requirements:
Package the temp snatching script in a docker container.
Write a gitlab ci pipeline that installs the docker and runs the script
- test - some linting
- build - build the actual docker image that will host the script
- run - run the script

Regarding the actual script there are a lot of ways we can do this.
Some are flashy and some are boring.
We can use lxml and requests, selenium, scrapy or even intercept ajax calls, but what i did was dig through the source and i ran into a page called:
https://weerindelft.nl/clientraw.txt

Here's how i got to it:

- view-source:https://www.weerindelft.nl/ pagesource -> to https://weerindelft.nl/WU/55ajax-dashboard-testpage.php
- https://weerindelft.nl/WU/55ajax-dashboard-testpage.php -> https://weerindelft.nl/WU/ajaxWDwx.js
- https://weerindelft.nl/WU/ajaxWDwx.js -> has the core functions and the most important one mentions clientraw

Indeed, there is a https://weerindelft.nl/clientraw.txt, and it updates on every GET, which is what we want.

var clientraw = x.responseText.split(' ');
    // now make sure we got the entire clientraw.txt
    // valid clientraw.txt has '12345' at start and '!!' at end of record
    var wdpattern=/\d+\.\d+.*!!/; // looks for '!!nn.nn!!' version string
...
temp = convertTemp(clientraw[4]); 

This makes a conversion from C to F but it's good enough for us to know the position of the temp, though we could have just guesses by looking at the string:

12345 0.0 0.0 129 11.6 91 981.7 1.4 12.6 [...] 13:20 08:04 129 !!C10.37S69!!

All we need for the temp is to get the url, split by spaces and get the 5th position (0 to 4).
