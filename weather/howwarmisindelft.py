#!/usr/bin/python

import requests
from sys import exit

try:
    page = requests.get('http://www.weerindelft.nl/clientraw.txt')
    # raise Error('It happened.')
    # -> we have to be really specific with error codes here
    #    so we can properly catch and handle them
    # -> we can mostly get away with not defining errors if using
    #    native libraries or good 3rd party ones but we have to be specific
    #    with in-house code
    raw = int(round(float(page.text.split(' ')[4])))
    print("%s degrees Celsius." % raw)
except Exception:
    print('Something went wrong.')
    exit(1)
